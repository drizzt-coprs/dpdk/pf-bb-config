Name:           pf-bb-config
Version:        22.03
Release:        2%{?dist}
Summary:        PF BBDEV (baseband device) Configuration Application

License:        ASL 2.0
URL:            https://github.com/intel/pf-bb-config
Source0:        https://github.com/intel/pf-bb-config/archive/refs/tags/v%{version}.tar.gz#/pf-bb-config-%{version}.tar.gz

BuildRequires:  gcc


%description
The PF BBDEV (baseband device) Configuration Application "pf_bb_config"
provides a means to configure the baseband device at the host-level.
The program accesses the configuration space and sets the various parameters
through memory-mapped IO read/writes.


%prep
%autosetup
sed -i "s/#VERSION_STRING#/%{version}/g" config_app.c


%build
%make_build CFLAGS="${RPM_OPT_FLAGS}" LDFLAGS="${RPM_LD_FLAGS}"


%install
install -d -m 755 %{buildroot}%{_bindir}
install -d -m 755 %{buildroot}%{_datadir}/pf-bb-config/acc100/
install -p -D -m 755 pf_bb_config %{buildroot}%{_bindir}/pf_bb_config
cp -a acc100/*.cfg %{buildroot}%{_datadir}/pf-bb-config/acc100/
# FIXME
ln -sf acc100_config_pf_4g5g.cfg %{buildroot}%{_datadir}/pf-bb-config/acc100/acc100_config.cfg


%files
%license LICENSE
%doc README.md
%{_bindir}/*
%{_datadir}/*


%changelog
* Tue Jun 28 2022 Timothy Redaelli <tredaelli@redhat.com> - 22.03-2
- Use version as #VERSION_STRING#

* Fri Jun 17 2022 Timothy Redaelli <tredaelli@redhat.com> - 22.03-1
- Initial commit
